class ChangePointsInPointLineItems < ActiveRecord::Migration
  def up
    change_column :point_line_items, :points, :integer, default: 0, null: false
  end

  def down
    change_column :point_line_items, :points, :string
  end
end
