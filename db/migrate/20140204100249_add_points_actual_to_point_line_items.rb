class AddPointsActualToPointLineItems < ActiveRecord::Migration
  def change
    add_column :point_line_items, :points_actual, :integer
  end
end
