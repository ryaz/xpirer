# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.find_or_create_by!(name: 'foobar')

PointLineItem.delete_all

[[25, 'Joined Loyalty Program', '01.01.2013', 25],
[410, 'Placed an order', '10.02.2013', 435],
[-250, 'Redeemed with an order', '15.02.2013', 185],
[10, 'Wrote a review', '18.02.2013', 195],
[570, 'Placed an order', '12.03.2013', 765],
[-500, 'Redeemed with an order', '15.04.2013', 265],
[130, 'Made a purchase', '27.06.2013', 395]].each do |row|
  PointLineItem.create!(points: row[0], source: row[1], created_at: row[2], user: user, points_actual: row[3])
end