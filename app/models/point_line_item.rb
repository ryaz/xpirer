class PointLineItem < ActiveRecord::Base
  belongs_to :user
  scope :by_period, ->(from, to) {where('created_at BETWEEN ? AND ?', from, to)}
end
