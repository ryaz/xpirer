class PointExpirer
  def self.expire(date) 
    if lis = PointLineItem.where("DATE(created_at) = ? AND points > 0", date.ago(1.year + 1.day).to_date) and lis.any?
      lis.each do |li|
        redeems = PointLineItem.where('created_at > ? AND points < 0 AND source = ?', li.created_at, 'Redeemed with an order')
        if redeems.any?
          if (li.points + redeems.map(&:points).sum > 0)
            PointLineItem.create(points: -redeems.first.points_actual, source: "Points ##{li.id} expired", user: li.user)
          end
        else
          PointLineItem.create(points: -li.points, source: "Points ##{li.id} expired", user: li.user)
        end
      end
    end
  end
end