require 'spec_helper'

describe PointExpirer do
  describe '.expire' do
    before do
      user = create :user
      [[25, 'Joined Loyalty Program', '01.01.2013', 25],
      [410, 'Placed an order', '10.02.2013', 435],
      [-250, 'Redeemed with an order', '15.02.2013', 185],
      [10, 'Wrote a review', '18.02.2013', 195],
      [570, 'Placed an order', '12.03.2013', 765],
      [-500, 'Redeemed with an order', '15.04.2013', 265],
      [130, 'Made a purchase', '27.06.2013', 395]].each do |row|
        create :point_line_item, points: row[0], source: row[1], created_at: Date.parse(row[2]), points_actual: row[3], user: user
      end
    end

    it 'expires 12/03/2013 item' do
      check_expired('13/03/2014', -265)
    end

    it 'expires 27/06/2013 item' do
      check_expired('28/06/2014', -130)
    end

    def check_expired(date, points)
      Timecop.freeze(Date.parse(date)) do
        PointExpirer.expire(Date.today)
        PointLineItem.last.points.should eq(points)
      end
    end
  end
end

