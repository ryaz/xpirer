FactoryGirl.define do
  factory :user do
    sequence(:name) {|n| "Bobby #{n}"}
  end
end